// the 'make.do' file is automatically created by 'github' package.
// execute the code below to generate the package installation files.
// DO NOT FORGET to update the version of the package, if changed!
// for more information visit http://github.com/haghish/github

make hdps,  toc pkg  version(1.0.0)                                          ///
     license("MIT")                                                          ///
     author("john tazare")                                                   ///
     affiliation("London School of Hygiene & Tropical Medicine")             ///
     email("john.tazare1@lshtm.ac.uk")                                       ///
     url("")                                                                 ///
     title("implementing hdps approaches")                                   ///
     description("")                                                         ///
     install("hdps_graphics.ado;hdps_prioritize.ado;hdps_prevalence.ado;hdps_recurrence.ado;hdps_setup.ado;hdps.ado") ///
     ancillary("")                                                         
